const AWS = require('aws-sdk')
const fs = require('fs')
const s3Config = require('../config/').s3

AWS.config.update({
    accessKeyId: s3Config.key,
    secretAccessKey: s3Config.secret,
    region: s3Config.region
})

const s3 = new AWS.S3({ apiVersion: '2006-03-01' })

class S3Upload {
    static upload(fileObj, objectKey, isPublic) {
        return new Promise((resolve, reject) => {
            const path = require('path')
            const filePath = path.join(global.APP_PATH, fileObj.image.tempFilePath)
            const readStream = fs.createReadStream(filePath)
            const chunks = []
            readStream.on('error', error => {
                console.error(error)
                reject({ CODE: 'ERROR_IN_UPLOAD' })
            })
            readStream.on('data', chunk => {
                chunks.push(chunk)
            })
            readStream.on('close', () => {
                const data = Buffer.concat(chunks)
                const s3Params = {
                    Bucket: s3Config.bucket,
                    Key: s3Config.publicPath + objectKey,
                    Body: data,
                    ContentType: fileObj.image.mimetype
                }
                s3Params.ACL = 'public-read'
                // Uploading files to the bucket
                s3.upload(s3Params, function (err, data) {
                    if (err) {
                        console.log(err)
                        reject({ CODE: 'ERROR_IN_UPLOAD' })
                    } else {
                        resolve(data.Location)
                    }
                })
            })
        })
    }
}

module.exports = S3Upload
