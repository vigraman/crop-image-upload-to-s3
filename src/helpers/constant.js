
export const IMG_RESOLUTION = [
    {
        id: "365H",
        w: 365,
        h: 212
    },
    {
        id: "380G",
        w: 380,
        h: 380
    },
    {
        id: "365V",
        w: 365,
        h: 450
    },
    {
        id: "755H", 
        w: 755,
        h: 450
    }
]