import React from 'react'
import './ImageUpload.css'
import {IMG_RESOLUTION} from '../../helpers/constant'
import {uploadToS3Public} from '../../service/api'


export default class ImageUpload extends React.Component {

    state = {
        imageUploadError: false,
        uploadedSuccessFully: false,
        "755H": "",
        "365V": "",
        "365H": "",
        "380G": ""
    }
      
    componentDidMount() {
        this.dragDropInit();
    }

    dragDropInit() {
        const dropWrapper = document.getElementById("dropzone-wrapper");
        dropWrapper.ondragover =(e) => {
            e.preventDefault();
            e.stopPropagation();
            dropWrapper.classList.add('dragover');
        };
        dropWrapper.ondragleave = (e) => {
            e.preventDefault();
            e.stopPropagation();
            dropWrapper.removeClass('dragover');
        };
    }

    uploadImage = async (input) => {
        const file = input.target.files && input.target.files[0];
        if(file) {
            const img = new Image();
            img.src = window.URL.createObjectURL( file );
            img.onload = async (el) => {
                const width = img.naturalWidth,
                    height = img.naturalHeight;
                if(width === 1024 && height === 1024) {
                    await IMG_RESOLUTION.map(async res => {
                        await this.compressImage(img, el, res).then(data => {
                            this.setState({[res.id]: data.data})
                        }).catch(err => {
                            console.log(err)
                        })
                    })
                    this.setState({uploadedSuccessFully: true})
                }
                else {
                    this.setState({imageUploadError: true});
                }
            };
        }
    }

    compressImage(img, el, res) {
        return new Promise((resolve, reject) => {
            const c = document.createElement("canvas");
            c.id = res.id;
            const ctx = c.getContext("2d");
            c.width = res.w;
            c.height = res.h;
            ctx.drawImage(img, (img.naturalWidth-res.w)/2, (img.naturalHeight-res.h)/2, res.w, res.h, 0, 0, res.w, res.h);
            const srcEncoded = ctx.canvas.toDataURL(el.target, 'image/jpeg', 0);
            this.convertToFile(srcEncoded, `${res.id}.jpg`).then(async data => {
                const fd = new FormData();
                await fd.append('image', data)
                await uploadToS3Public(fd).then(data => {
                    resolve(data)
                }).catch(err => {
                    console.log(err)
                })
            }).catch(err => {
                console.log(err);
            })
        })
    }

    convertToFile = (base64, filename) => {
        return (fetch(base64)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename,{type:'image/jpeg'});})
    );
    }

    render() {
        const { uploadedSuccessFully, imageUploadError } = this.state;
        return (
            <div>
                <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label className="control-label">Upload 1024 X 1024 Image</label>
                                    {
                                        uploadedSuccessFully ? 
                                        <div className="preview-zone hidden">
                                            <div className="box box-solid">
                                                <div className="box-header with-border">
                                                    <div><b>Preview</b></div>
                                                    <div id="preview-image">
                                                        {
                                                            IMG_RESOLUTION.map(res => 
                                                                <div key={res.id} className="image-container" style={{width: res.w}}>
                                                                    <img src={this.state[res.id]} alt={res.id}/>
                                                                    <div className="text-block">
                                                                        <h4>{`${res.w} X ${res.h}`}</h4>
                                                                    </div>
                                                                </div>
                                                            )
                                                        }
                                                    </div>
                                                </div>
                                                <div className="box-body"></div>
                                            </div>
                                        </div> 
                                        :
                                        <div className="dropzone-wrapper" id="dropzone-wrapper">
                                            <div className="dropzone-desc">
                                                <i className="glyphicon glyphicon-download-alt"></i>
                                                <p>Choose an image file or drag it here.</p>
                                            </div>
                                            <input type="file" name="img_logo" onChange={this.uploadImage} accept="image/*" className="dropzone" id="dropzone-input"/>
                                        </div>
                                    }
                                    {imageUploadError ? <div style={{color: "red"}}>Image Doesn't Meet the requirement(ONly Upload 1024X1024</div> : null}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}