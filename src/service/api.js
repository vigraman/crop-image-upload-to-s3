import {create} from "apisauce";


const local = create({
    baseURL: '/',
    headers: {
        'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8',
        'Accept' : '*/*'
    },
    timeout: 25000
})

export const uploadToS3Public = (formData = {}) => local.post('/uploadS3/public', formData)
