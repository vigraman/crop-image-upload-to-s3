module.exports = {
  apps : [{
    name: 'IMAGE_UPLOAD',
    script: 'index.js',
    args: 'one two',
    instances: 0,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env_test: {
      NODE_ENV: 'test'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    test : {
      user : 'ubuntu',
      key  : '~/Downloads/AWS.pem',
      host : '13.235.247.127',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:vigraman/crop-image-upload-to-s3.git',
      path : '/var/www/image-upload',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env test',
      'pre-setup': ''
    },
    production : {
      user : 'ubuntu',
      key  : '~/Downloads/AWS.pem',
      host : '13.235.247.127',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:vigraman/crop-image-upload-to-s3.git',
      path : '/home/ubuntu/image-upload',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && npm run build && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
